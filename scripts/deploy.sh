#!/usr/bin/env bash

#
# Deploy with rsync
#

set -e

source .deploy.config

rsync -av --rsh="ssh -p$DEPLOY_PORT" dist/ "$DEPLOY_USER@$DEPLOY_HOST:$DEPLOY_PATH"
