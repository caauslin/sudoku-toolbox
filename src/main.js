import Vue from 'vue'
import VueRouter from 'vue-router'
import App from '@/App.vue'

Vue.config.productionTip = false

Vue.use(VueRouter)

new Vue({
  router: new VueRouter({
    mode: 'history',
  }),
  render: h => h(App),
}).$mount('#app')
